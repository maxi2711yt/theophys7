import matplotlib.pyplot as plt
from mpl_toolkits.axisartist.axislines import AxesZero
import csv

#domain = [-10,10,-10,10]
domain = True

print("Loading data...")

with open("../data.csv", "r") as file:
    reader = csv.reader(file, delimiter=';')
    data = list(reader)

print("Data loaded!")

x0 = []
y0 = []

x1 = []
y1 = []

x2 = []
y2 = []

print("Organizing data...")

for list in data:
    x0.append(float(list[0]))
    y0.append(float(list[1]))
    x1.append(float(list[2]))
    y1.append(float(list[3]))
    x2.append(float(list[4]))
    y2.append(float(list[5]))

print("Data organized!\nPreparing plot...")

fig = plt.figure()
ax = fig.add_subplot(axes_class=AxesZero)

color1 = "r"
color2 = "g"
color3 = "b"

plt.plot(x0, y0, color = color1)
plt.plot(x1, y1, color = color2)
plt.plot(x2, y2, color = color3)
plt.grid()
plt.axis(True)
plt.axis(domain) 
ax.axis["xzero"].set_axisline_style("-|>")
ax.axis["yzero"].set_axisline_style("-|>")
ax.axis["xzero"].set_visible(True)
ax.axis["yzero"].set_visible(True)
ax.set_aspect('equal')

print("Calculating arrows!")
size = max(abs(max(x0)),abs(max(x1)),abs(max(x2)),abs(max(y1)),abs(max(y2)),abs(max(y0))) / 50
n = 10
stepsize = int(len(x0) / n);
for i in range(0, n):
    plt.arrow(x0[i * stepsize], y0[i * stepsize], x0[i * stepsize + 1] - x0[i * stepsize], y0[i * stepsize + 1] - y0[i * stepsize], shape="full", lw=0, length_includes_head=True , head_width=size, color=color1); 
    plt.arrow(x1[i * stepsize], y1[i * stepsize], x1[i * stepsize + 1] - x1[i * stepsize], y1[i * stepsize + 1] - y1[i * stepsize], shape="full", lw=0, length_includes_head=True , head_width=size, color=color2); 
    plt.arrow(x2[i * stepsize], y2[i * stepsize], x2[i * stepsize + 1] - x2[i * stepsize], y2[i * stepsize + 1] - y2[i * stepsize], shape="full", lw=0, length_includes_head=True , head_width=size, color=color3); 

print("Done!\nShowing Plot!")
plt.savefig("plot.png", dpi=400, bbox_inches='tight', pad_inches=0.15)
plt.show()

