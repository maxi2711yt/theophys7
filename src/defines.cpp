#include "../include/defines.hpp"

[[nodiscard]] double d_abs(const double a)
{
    if(a < 0) return -a;
    return a;
}

[[nodiscard]] double d_pow(const double in, const size_t pow)
{
    if(in == 0) return 0.0f;
    if(pow == 0) return 1.0f;
    double out = 1;
    size_t i = 0;
    do 
    {
        out *= in;
        i++;
    } while(i < pow);
    return out;
}


[[nodiscard]] vec2d v_add(const vec2d& a, const vec2d& b)
{
    return vec2d{ a.x + b.x, a.y + b.y };
}

[[nodiscard]] vec2d v_sub(const vec2d& a, const vec2d& b)
{
    return vec2d{ a.x - b.x, a.y - b.y };
}

void v_scale(vec2d& vec, const double scale)
{
    vec = vec2d{ vec.x * scale, vec.y * scale };
}

[[nodiscard]] vec2d v_scaleR(vec2d vec, const double scale)
{
    v_scale(vec, scale);
    return vec;
}

[[nodiscard]] double v_dotP(const vec2d& veca, const vec2d& vecb)
{
    return veca.x * vecb.x + veca.y * vecb.y;
}

[[nodiscard]] double d_sqrt(const double a)
{
    double out = a;
    for(size_t i = 0; i < STEPS; i++)
    {
        out = out - (d_pow(out, 2) - a)/(2 * out);
    }
    return out;
}

[[nodiscard]] double v_norm(const vec2d& vec)
{
    return d_sqrt(v_dotP(vec,vec));
}

[[nodiscard]] std::string v_string(const vec2d& a)
{
    std::string out = "(";
    out += std::to_string(a.x);
    out += '|';
    out += std::to_string(a.y);
    out += ')';
    return out;
}
