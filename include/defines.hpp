#pragma once

#include <string>
#define STEPS 20

typedef struct VECTOR2D {
    double x;
    double y;
} vec2d;

[[nodiscard]] double d_abs(const double a); // Returns the absolute value of a
[[nodiscard]] double d_pow(const double in, const size_t pow); // Calculates in^pow
[[nodiscard]] double d_sqrt(const double in); // Calculates the square root of a number

[[nodiscard]] vec2d v_add(const vec2d& a, const vec2d& b); // Adds to vectors and returns the result
[[nodiscard]] vec2d v_sub(const vec2d& a, const vec2d& b); // Subtracts to vectors and returns the result
void v_scale(vec2d& vec, const double scale); // Scales a vector using a constant
[[nodiscard]] vec2d v_scaleR(vec2d vec, const double scale); // Scales a vector and retuns it

[[nodiscard]] double v_dotP(const vec2d& veca, const vec2d& vecb); // Retuns the dot product of two vectos
[[nodiscard]] double v_norm(const vec2d& vec); // Retuns the norm of a vector

[[nodiscard]] std::string v_string(const vec2d& a); // Returns a string represantation of the vecor as (x,y)
