import matplotlib.pyplot as plt
from mpl_toolkits.axisartist.axislines import AxesZero
import csv

with open("../energies.csv", "r") as file:
    reader = csv.reader(file, delimiter=';')
    data = list(reader)

t = []
V = []
T1 = []
T2 = []
T3 = []

E = []

for list in data:
    V.append(float(list[0]))
    T1.append(float(list[1]))
    T2.append(float(list[2]))
    T3.append(float(list[3]))
    t.append(float(list[4]))

for i in range(len(V)):
    E.append(V[i] + T1[i] + T2[i] + T3[i])

plt.plot(t, V)
plt.plot(t, T1)
plt.plot(t, T2)
plt.plot(t, T3)
plt.plot(t, E)

plt.show()
