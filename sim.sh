#!/bin/bash

echo "Compiling..."
./compile_clang.sh
echo "Running simulation..."
./out
echo "Creating video and plot..."
cd dataplotting
# python animatecurves.py
python plotcurves.py

