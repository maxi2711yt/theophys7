This repo contains the code for an exercice in Theo. Phys. I at Humboldt University Berlin.

Example simulation outcomes are provided in the pictures in the root directory.

# Compiling and running the code

1. Download the repo to a machine with a C++ compiler (GCC or Clang are recommended), preferably a device running linux. The command for this is
```git clone https://www.gitlab.com/maxi2711yt/phystheo7```
2. Run one of the compile scripts. This will generate an executable `out`.
3. Run the executable.

The program will output the results of the calculation as a table to the console aswell as a CSV file named `data.csv`.

To display the output just enter the `dataplotting` folder and run the python file inside it. Make sure you have both python 3.7, or newer, and matplotlib installed.

# Adjusting the simulation

The program is build in a way, which makes it possible to change the starting positions of all bodies, aswell as their starting velocity and accellaration. To do so enter the `main.cpp` file and adjust the values in lines 41-51.
The starting masses of the bodies can be adjusted via the compile time constants situated the beginning of file named `MASS0`, `MASS1`, `MASS2`.
Increasing or decreasing the timestep can be done via adjusting the `TIMESTEP` compile time constant situated the beginning of the file.
The endtime can be adjusted by adjusting the `TEND` constant at the beginning of the file.
