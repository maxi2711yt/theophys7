#include <iostream>

#define FAST_TABLE
#define DATA_TABLE
#define SAVE_MEM


#include "../include/defines.hpp"
#include "../include/table.hpp"


#define MASS0 5
#define MASS1 3
#define MASS2 4
#define STEPSIZE 0.000001f 
#define TEND 10.0f

#define DENSITY 24
#define AUTO_DATA_POINT_COUNT

#ifdef AUTO_DATA_POINT_COUNT
    #define DATA_POINT_COUNT (int)(TEND * DENSITY)
#else
    #define DATA_POINT_COUNT 1000
#endif

#define EXPONENT 3

using namespace types;

vec2d calcAcc(const vec2d& a, const vec2d& b, const vec2d& c, const double massb, const double massc) // Calculates the acceleration experienced at veca 
{
    vec2d out = { };

    out.x = - massb * (a.x - b.x)/(d_pow(v_norm(v_sub(a,b)), EXPONENT)) - massc * (a.x - c.x)/(d_pow(v_norm(v_sub(a,c)), EXPONENT)); 
    out.y = - massb * (a.y - b.y)/(d_pow(v_norm(v_sub(a,b)), EXPONENT)) - massc * (a.y - c.y)/(d_pow(v_norm(v_sub(a,c)), EXPONENT)); 

    return out;
}

int main()
{
    // Defining the output table
    Table<double> outputtable(1,6), vel(1,6);
    
    // Defining the vectors for the bodies
    vec2d x0 = {1, -1};
    vec2d v0 = {0,  0};
    vec2d a0 = {0,  0};

    vec2d x1 = {1,  3};
    vec2d v1 = {0,  0};
    vec2d a1 = {0,  0};

    vec2d x2 = {-2,-1};
    vec2d v2 = {0,  0};
    vec2d a2 = {0,  0};

    // Running the simulation
    double t = 0;
    int row = outputtable.rows() - 1;
    
    size_t steps = (size_t)(TEND / STEPSIZE);
    size_t curr_step = 0;
    char percent = 0;
    std::cout << "Running the simulation" << std::endl;
    while(t < TEND)
    {
        // Calculating accelerations
        a0 = calcAcc(x0, x1, x2, MASS1, MASS2);
        a1 = calcAcc(x1, x0, x2, MASS0, MASS2);
        a2 = calcAcc(x2, x0, x1, MASS0, MASS1);

        // Calculating speeds 
        v0 = v_add(v0, v_scaleR(a0, STEPSIZE));
        v1 = v_add(v1, v_scaleR(a1, STEPSIZE));
        v2 = v_add(v2, v_scaleR(a2, STEPSIZE));

        // Calculating new positions
        x0 = v_add(x0, v_add(v_scaleR(a0, d_pow(STEPSIZE, 2)), v_scaleR(v0, STEPSIZE)));
		x1 = v_add(x1, v_add(v_scaleR(a1, d_pow(STEPSIZE, 2)), v_scaleR(v1, STEPSIZE)));
		x2 = v_add(x2, v_add(v_scaleR(a2, d_pow(STEPSIZE, 2)), v_scaleR(v2, STEPSIZE)));
        
#ifdef SAVE_MEM
        if(curr_step % (steps / DATA_POINT_COUNT) == 0 ) {
            std::cout << "Saving data point at time " << t << " into the tables.\n";
#endif
        if(row >= 1){
            outputtable.addRow();
            vel.addRow();
        }

        // Saving the speeds
        vel.setElement(row, 0, v0.x);
        vel.setElement(row, 1, v0.y);
        vel.setElement(row, 2, v1.x);
        vel.setElement(row, 3, v1.y);
        vel.setElement(row, 4, v2.x);
        vel.setElement(row, 5, v2.y);

        // Adding data to table
        outputtable.setElement(row, 0, x0.x);
        outputtable.setElement(row, 1, x0.y);
        outputtable.setElement(row, 2, x1.x);
        outputtable.setElement(row, 3, x1.y);
        outputtable.setElement(row, 4, x2.x);
        outputtable.setElement(row, 5, x2.y);
        
        row++;
#ifdef SAVE_MEM
        }
#endif 
        t += STEPSIZE;
        if(curr_step % (steps / 100) == 0)
        {
            std::cout << (int)percent << "% complete! Currently at time " << t << "\n";
            percent++;
        }
        curr_step++;
    }

    std::cout << "Exporting data...\n";
    // Drawing the table to stdout
    // outputtable.automaticallySizeColumns();
    outputtable.drawTable();
    // Exporting the tables as a CSV
    outputtable.exportTableAsCSV("data.csv", DEFAULT_CSV_DIV_CHAR);
    vel.exportTableAsCSV("vel.csv", DEFAULT_CSV_DIV_CHAR);
    return 0;
}
