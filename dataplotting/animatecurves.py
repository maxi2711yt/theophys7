import matplotlib.pyplot as plt
from mpl_toolkits.axisartist.axislines import AxesZero
import csv
import os
import multiprocessing

bitrate = 5000 # k bits
framerate = 12
length = 20
frames = framerate * length
domain = [-10,10,-10,10]
center_view_port = True
auto_dimension = True 
view_port_dim = 10
process_count = multiprocessing.cpu_count()
draw_arrows = True
arrow_scale = 1
arrow_width = 0.005
head_scale = 10
scatter_dot_size = 3

print("Loading Data...")
with open("../data.csv", "r") as file:
    reader = csv.reader(file, delimiter=';')
    datapos = list(reader)

if draw_arrows:
    with open("../vel.csv", "r") as file:
        reader = csv.reader(file, delimiter=';')
        datavel = list(reader)

print("Loaded Data!")
print("Organizing Data...")
x0 = []
y0 = []
x1 = []
y1 = []
x2 = []
y2 = []

v0x = []
v1x = []
v2x = []
v0y = []
v1y = []
v2y = []

for list in datapos:
    x0.append(float(list[0]))
    y0.append(float(list[1]))
    x1.append(float(list[2]))
    y1.append(float(list[3]))
    x2.append(float(list[4]))
    y2.append(float(list[5]))
del datapos[:]
if draw_arrows:
    for list in datavel:
        v0x.append(float(list[0]))
        v0y.append(float(list[1]))
        v1x.append(float(list[2]))
        v1y.append(float(list[3]))
        v2x.append(float(list[4]))
        v2y.append(float(list[5]))
    del datavel[:] 

print("Organized Data!")

intervall = int(len(x0) / frames)

color1 = "r"
color2 = "g"
color3 = "b"

os.system("rm -dr output")
os.system("mkdir output")

print("Creating images...")

def calc_viewport(xa, ya, xb, yb, xc, yc):
    x = (xa + xb + xc) / 3
    y = (ya + yb + yc) / 3
    if auto_dimension == True: 
        temp = max(abs(max(xa,xb,xc) - min(xa,xb,xc)), abs(max(ya,yb,yc) - min(ya,yb,yc)))
        global view_port_dim
        view_port_dim = temp * 2
    return [x - (view_port_dim / 2), x + (view_port_dim / 2), y - (view_port_dim / 2), y + (view_port_dim / 2)]

def animate(start, stop, id):
    for i in range(start, stop):
        # subfig, subax = plt.subplots()
        plt.figure(id).clear()
        fig = plt.figure(id)
        ax = fig.add_subplot(axes_class=AxesZero)
        
        ax.set_aspect('equal')
        ax.axis["xzero"].set_axisline_style("-|>")
        ax.axis["yzero"].set_axisline_style("-|>")
        ax.axis["xzero"].set_visible(True)
        ax.axis["yzero"].set_visible(True)

        plt.plot(x0[i * intervall], y0[i * intervall], marker="o", color=color1, markersize=scatter_dot_size)
        plt.plot(x1[i * intervall], y1[i * intervall], marker="o", color=color2, markersize=scatter_dot_size)
        plt.plot(x2[i * intervall], y2[i * intervall], marker="o", color=color3, markersize=scatter_dot_size)
        
        if draw_arrows:
            plt.arrow(x0[i * intervall], y0[i * intervall], v0x[i * intervall] * arrow_scale, v0y[i * intervall] * arrow_scale, length_includes_head=True, color=color1, shape='full', width=arrow_width, head_width=arrow_width * head_scale)
            plt.arrow(x1[i * intervall], y1[i * intervall], v1x[i * intervall] * arrow_scale, v1y[i * intervall] * arrow_scale, length_includes_head=True, color=color2, shape='full', width=arrow_width, head_width=arrow_width * head_scale)
            plt.arrow(x2[i * intervall], y2[i * intervall], v2x[i * intervall] * arrow_scale, v2y[i * intervall] * arrow_scale, length_includes_head=True, color=color3, shape='full', width=arrow_width, head_width=arrow_width * head_scale)

        plt.grid()
        plt.axis(True)
        if center_view_port:
            plt.axis(calc_viewport(x0[i * intervall], y0[i * intervall], x1[i * intervall], y1[i * intervall], x2[i * intervall], y2[i * intervall]))
        else:
            plt.axis(domain)
        
        plt.savefig("./output/{index}.png".format(index=i), dpi=150, bbox_inches="tight", pad_inches=0.15)
        print("{id}: {index}/{count} images rendered and saved!".format(id=id, index=i % (stop - start), count=stop-start))
    return

process_range = int(frames / process_count)
processes = []

for i in range(process_count):
    x = (multiprocessing.Process(target=animate,args=(i * process_range, (i + 1) * process_range, i)))
    processes.append(x)
    x.start()

if frames % process_count != 0:
    x = (multiprocessing.Process(target=animate, args=(frames - frames % process_count, frames, len(processes))))
    processes.append(x)
    x.start()

for process in processes:
    process.join()

os.system("ffmpeg -y -r {fps} -i ./output/%d.png -vcodec mpeg4 -b:v {bit}k ./output/plot.mp4".format(fps=framerate, bit=bitrate))
